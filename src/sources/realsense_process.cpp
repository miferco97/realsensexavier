#include "realsense_process.h"
// #include "detector_wrapper.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>


using namespace rs2;
using namespace cv;
using namespace std;


RealsenseProcess::RealsenseProcess():RobotProcess(),align_to_color(RS2_STREAM_COLOR)/*,ball_detect()*/{

}
RealsenseProcess::~RealsenseProcess(){

}

//ownSetUp()
void RealsenseProcess::ownSetUp()
{

    config_rs.enable_stream(rs2_stream::RS2_STREAM_DEPTH, width, height, rs2_format::RS2_FORMAT_Z16);
    config_rs.enable_stream(rs2_stream::RS2_STREAM_COLOR, width, height, rs2_format::RS2_FORMAT_RGB8);

    pub_depth = node.advertise<sensor_msgs::Image>(depth_topic_name, 10);
    pub_rgb = node.advertise<sensor_msgs::Image>(rgb_topic_name, 10);


}


//ownStart()
void RealsenseProcess::ownStart()
{
    pipe.start(config_rs);
}

//ownStop()
void RealsenseProcess::ownStop()
{
}

//ownRun()
void RealsenseProcess::ownRun()
{
    rs2::colorizer c;                     // Helper to colorize depth images

    frameset = pipe.wait_for_frames();
    frameset = align_to_color.process(frameset);
    auto depth = frameset.get_depth_frame();
    auto color = frameset.get_color_frame();
    auto colorized_depth = c.colorize(depth);

    Mat image_c(Size(width, height), CV_8UC3, (void*)color.get_data(), Mat::AUTO_STEP);
    Mat image_d(Size(width, height), CV_8UC3, (void*)colorized_depth.get_data(), Mat::AUTO_STEP);
    Mat image_di(Size(width, height),CV_16U, (void*)depth.get_data(), Mat::AUTO_STEP);

    // cvtColor(image, frame_HSV, COLOR_BGR2HSV);

    cv_bridge::CvImage out_rgb;
    // cv_bridge::CvImage out_depth_colorized;
    cv_bridge::CvImage out_depth;

    // out_msg.header   = in_msg->header; // Same timestamp and tf frame as input image
    out_rgb.encoding = sensor_msgs::image_encodings::BGR8; // Or whatever
    out_rgb.image    = image_c; // Your cv::Mat

    out_depth.encoding = sensor_msgs::image_encodings::MONO16; // Or whatever
    out_depth.image    = image_di; // Your cv::Mat

    // if (ball_detect.process_frame(image_c) == -1) exit(0);

    pub_rgb.publish(out_rgb.toImageMsg());
    pub_depth.publish(out_depth.toImageMsg());


}
