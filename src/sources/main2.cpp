#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <algorithm>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
using namespace cv;
using namespace std;
void trackbar_func(int position, void *userdata)
{
    *((int *)&userdata) = position;
}
int low_H;
int high_H;
int low_S;
int high_S;
int low_V;
int high_V;
int dilation;
int erosion;
Mat threshold(Mat image)
{
    Mat frame_HSV, BW;
    // image=equalizeIntensity(image);
    cvtColor(image, frame_HSV, COLOR_BGR2HSV);
    if (low_H < high_H)
        inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(high_H, high_S, high_V), BW);
    else
    {
        Mat BW1, BW2;
        inRange(frame_HSV, Scalar(0, low_S, low_V), Scalar(high_H, high_S, high_V), BW1);
        inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(180, high_S, high_V), BW2);
        BW = BW1 + BW2;
    }
    if (dilation)
        dilate(BW, BW, Mat(), Point(-1, -1), dilation, 1, 1);
    if (erosion)
        erode(BW, BW, Mat(), Point(-1, -1), erosion, 1, 1);
    return BW;
}
bool compareContours(vector<Point> a, vector<Point> b)
{
    Rect bb1 = boundingRect(a);
    Rect bb2 = boundingRect(b);
    return bb1.height * bb1.width > bb2.height * bb2.width;
}
bool detect_ball(const cv::Mat &im, cv::Point2f &center, float &radius)
{
    bool flag = false;
    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;
    cv::findContours(im, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
    if (contours.size())
    {
        flag = true;
        sort(contours.begin(), contours.end(), compareContours);
        auto c = contours[0];
        minEnclosingCircle(c, center, radius);
    }
    return flag;
}
int main(void)
{
    namedWindow("Configuration window");
    createTrackbar("Low H", "Configuration window", &low_H, 180, trackbar_func, &low_H);
    createTrackbar("High H", "Configuration window", &high_H, 180, trackbar_func, &high_H);
    createTrackbar("Low S", "Configuration window", &low_S, 255, trackbar_func, &low_S);
    createTrackbar("High S", "Configuration window", &high_S, 255, trackbar_func, &high_S);
    createTrackbar("Low V", "Configuration window", &low_V, 255, trackbar_func, &low_V);
    createTrackbar("High V", "Configuration window", &high_V, 255, trackbar_func, &high_V);
    createTrackbar("Erosion", "Configuration window", &erosion, 20, trackbar_func, &erosion);
    createTrackbar("Dilation", "Configuration window", &dilation, 20, trackbar_func, &dilation);
    VideoCapture cap(1);
    // Check if camera opened successfully
    if (!cap.isOpened())
    {
        cout << "Error opening video stream or file" << endl;
        return -1;
    }
    while (1)
    {
        Mat frame;
        // Capture frame-by-frame
        cap >> frame;
        // If the frame is empty, break immediately
        if (frame.empty())
            break;
        // Display the resulting frame
        Mat BW = threshold(frame);
Point2f center;
float radius;
if(detect_ball(BW, center, radius)){
circle(frame,center,radius, Scalar(150,0,0), 3);
}
                imshow("Frame", frame);
        imshow("Configuration window", BW);
        // Press  ESC on keyboard to exit
        char c = (char)waitKey(25);
        if (c == 27)
            break;
    }
    // When everything done, release the video capture object
    cap.release();
    // Closes all the frames
    destroyAllWindows();
    return 0;
}
