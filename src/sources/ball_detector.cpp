#include "ball_detector.h"



using namespace cv;
using namespace std;

void trackbar_func(int position, void *userdata)
{
	*((int *)&userdata) = position;
}

BallDetector::BallDetector(){

	namedWindow(window_name);
	createTrackbar("Low H", window_name, &low_H, 180, trackbar_func, &low_H);
	createTrackbar("High H", window_name, &high_H, 180, trackbar_func, &high_H);
	createTrackbar("Low S", window_name, &low_S, 255, trackbar_func, &low_S);
	createTrackbar("High S", window_name, &high_S, 255, trackbar_func, &high_S);
	createTrackbar("Low V", window_name, &low_V, 255, trackbar_func, &low_V);
	createTrackbar("High V", window_name, &high_V, 255, trackbar_func, &high_V);
	createTrackbar("Erosion", window_name, &erosion, 20, trackbar_func, &erosion);
	createTrackbar("Dilation", window_name, &dilation, 20, trackbar_func, &dilation);
	dilation=3;
	erosion=3;

}

BallDetector::~BallDetector(){

	destroyAllWindows();

}


Mat BallDetector::threshold(Mat image)
{

	// std::cout << "aqui1" << '\n';
	Mat frame_HSV, BW;
	// image=equalizeIntensity(image);
	cvtColor(image, frame_HSV, COLOR_BGR2HSV);
	if (low_H < high_H)
	inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(high_H, high_S, high_V), BW);
	else
	{
		Mat BW1, BW2;
		inRange(frame_HSV, Scalar(0, low_S, low_V), Scalar(high_H, high_S, high_V), BW1);
		inRange(frame_HSV, Scalar(low_H, low_S, low_V), Scalar(180, high_S, high_V), BW2);
		BW = BW1 + BW2;
	}
	// std::cout << dilation << '\n';

	// dilation =3;
	if (erosion) 	erode(BW, BW, getStructuringElement(MORPH_RECT, Size(erosion, erosion)));
	if (dilation) 	dilate(BW, BW, getStructuringElement(MORPH_RECT, Size(dilation,dilation)));
	return BW;
}

bool compareContours(vector<Point> a, vector<Point> b)
{
	Rect bb1 = boundingRect(a);
	Rect bb2 = boundingRect(b);
	return bb1.height * bb1.width > bb2.height * bb2.width;
}


bool BallDetector::detect_ball(const cv::Mat &im, cv::Point2f &center, float &radius)
{
	bool flag = false;
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;

	cv::findContours(im, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	if (contours.size())
	{
		flag = true;
		sort(contours.begin(), contours.end(), compareContours);
		auto c = contours[0];
		minEnclosingCircle(c, center, radius);
	}
	return flag;
}


int BallDetector::process_frame(Mat frame){

	cvtColor(frame, frame, cv::COLOR_BGR2RGB);


	if (frame.empty())
	return -1;

	// Display the resulting frame
	Mat BW = threshold(frame);

	Point2f center;
	float radius;

	if(detect_ball(BW, center, radius)){
		// circle(frame,center,radius, Scalar(150,0,0), 3);
		circle(BW,center,radius, Scalar(150,0,0), 3);
	}

	std::cout << center << '\n';
	// imshow("Frame", frame);
	imshow(window_name,cv::Mat::zeros(cv::Size(900,1),CV_8UC3)) ;
	imshow("Frame", BW);
	// Press  ESC on keyboard to exit

	waitKey(25);
	// char c = (char)waitKey(25);
	// if (c == 27)
	//  	break;
	return 1;
}


std::list<Detection> BallDetector::detect(cv::Mat image){
	std::list<Detection> detections;
	std::vector<bbox_t> result_vec;


	result_vec = detector.detect(image, this->threshold);
	for(auto it = result_vec.begin(); it<result_vec.end(); it ++){
		detections.push_back(Detection(*it) );
	}

	detections.sort();
	return detections;

};
