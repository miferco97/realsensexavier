#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include<vector>
#include<cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>

#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>   // Include OpenCV API
#include <chrono>

using namespace cv;
using namespace std;



int main(int argc, char * argv[])
{
    // Create and initialize GUI related objects
    rs2::colorizer c;                     // Helper to colorize depth images
    // texture depth_image, color_image;     // Helpers for renderig images

    // Create a pipeline to easily configure and start the camera
    rs2::pipeline pipe;
    rs2::config cfg;
    int width = 1280;
    int height = 720;

    cfg.enable_stream(rs2_stream::RS2_STREAM_DEPTH, width, height, rs2_format::RS2_FORMAT_Z16);
    cfg.enable_stream(rs2_stream::RS2_STREAM_COLOR, width, height, rs2_format::RS2_FORMAT_RGB8 );
    pipe.start(cfg);

    // Define two align objects. One will be used to align
    // to depth viewport and the other to color.
    // Creating align object is an expensive operation
    // that should not be performed in the main loop

    rs2::align align_to_color(RS2_STREAM_COLOR);
    auto start = chrono::steady_clock::now();

    while (1) // Application still alive?
    {

        // Using the align object, we block the application until a frameset is available

        rs2::frameset frameset = pipe.wait_for_frames();
        frameset = align_to_color.process(frameset);

        // Align all frames to color viewport
        start = chrono::steady_clock::now();

        // With the aligned frameset we proceed as usual

        auto depth = frameset.get_depth_frame();
        auto color = frameset.get_color_frame();
        auto colorized_depth = c.colorize(depth);

        Mat image_c(Size(width, height), CV_8UC3, (void*)color.get_data(), Mat::AUTO_STEP);
        Mat image_d(Size(width, height), CV_8UC3, (void*)colorized_depth.get_data(), Mat::AUTO_STEP);
        Mat image_di(Size(width, height),CV_16U, (void*)depth.get_data(), Mat::AUTO_STEP);

        // cout <<"distance : " << depth.get_distance(20,20) << "m ,  image depth ="<< image_di.at <unsigned short> (20,20) << " m "<<endl;

        cvtColor(image_c, image_c, cv::COLOR_BGR2RGB);

        auto end = chrono::steady_clock::now();

        cout << "Elapsed time in milliseconds : "
            << chrono::duration_cast<chrono::milliseconds>(end - start).count()
            << " ms" << endl;

            // start=end;

        /*
        if (dir == direction::to_depth)
        {
        // When aligning to depth, first render depth image
        // and then overlay color on top with transparancy
        depth_image.render(colorized_depth, { 0, 0, app.width(), app.height() });
        color_image.render(color, { 0, 0, app.width(), app.height() }, alpha);
    }
    else
    {
    // When aligning to color, first render color image
    // and then overlay depth image on top
    color_image.render(color, { 0, 0, app.width(), app.height() });
    depth_image.render(colorized_depth, { 0, 0, app.width(), app.height() }, 1 - alpha);
}
*/ image_di.convertTo(image_di, CV_8U,0.00390625);
// image_di=image_di / 255;//*(255.0/(1000.0*2.0));
cout <<  "image depth ="<< image_di.at <unsigned short> (20,20) << " m "<<endl;
imshow("Color", image_c);
imshow("Depth", image_d);
imshow("Depth1", image_di);//*255/30.0);

waitKey(1);


}

return EXIT_SUCCESS;
}
