#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
#include <algorithm>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <iostream>
#include "detector_wrapper.h"
#include <memory>
#include <string>
#include<list>
#include<vector>


using namespace cv;
using namespace std;


class BallDetector : public DetectorWrapper{
private:
	int low_H, high_H;
	int low_S, high_S;
	int low_V, high_V;
	int dilation, erosion;
	string window_name = "Configuration window";

public:
	BallDetector();
	~BallDetector();
	Mat threshold(Mat image);
	bool detect_ball(const cv::Mat &im, cv::Point2f &center, float &radius);
	int process_frame(Mat frame);
	std::list<Detection> detect(cv::Mat image);



};
