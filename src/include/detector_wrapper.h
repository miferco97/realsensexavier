#ifndef __DETECTOR_WRAPPER__
#define __DETECTOR_WRAPPER__

#include <iostream>
#include <memory>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
#include<list>
#include<vector>

using namespace std;

struct bbox_t {
    unsigned int x, y, w, h;       // (x,y) - top-left corner, (w, h) - width & height of bounded box
    float prob;                    // confidence - probability that the object was found correctly
    unsigned int obj_id;           // class of object - from range [0, classes-1]
    unsigned int track_id;         // tracking id for video (0 - untracked, 1 - inf - tracked object)
    unsigned int frames_counter;   // counter of frames on which the object was detected
    float x_3d, y_3d, z_3d;        // center of object (in Meters) if ZED 3D Camera is used
};

struct Detection{
	Detection() {}

	Detection(bbox_t b){
		class_name = "";
		x=b.x;
		y=b.y;
		w=b.w;
		h=b.h;
		c=b.obj_id;
		confidence= b.prob;
	}
    
	float x,y,w,h,confidence;
	int c;
	bool operator < (const Detection &d2) const{
		return this->confidence <= d2.confidence;
	}
	string class_name;
	bool has_pose() const {return pose;}
	bool has_detection() const {return detection;}
	float x_p, y_p, z_p, roll_p, pitch_p , yaw_p;
	bool pose=false, detection;

};

class DetectorWrapper {
public:
	DetectorWrapper(){};
	virtual void setScale(int scale) {this->scale = scale ; }
	virtual int getScale () {return this-> scale; };
	virtual std::list<Detection> detect(cv::Mat image){};
	virtual std::vector<std::string> getClasses() const { return std::vector<std::string>();};
	virtual void setThreshold (float t){threshold = t;}
protected:
	float threshold=0.1f;
	int scale ;
};

#endif
