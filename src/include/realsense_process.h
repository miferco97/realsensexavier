#ifndef _REALSENSE_PROCESS
#define _REALSENSE_PROCESS

#include "robot_process.h"
#include "ros/ros.h"
#include "sensor_msgs/Image.h"
#include <librealsense2/rs.hpp>
// #include "ball_detector.h"

// #include<librealsense2/rs_sensor.h>

#include <opencv2/opencv.hpp>
using namespace rs2;

class RealsenseProcess: public RobotProcess {
public:

    RealsenseProcess();
    ~RealsenseProcess();

    void ownSetUp();
    void ownStart();
    void ownStop();
    void ownRun();



private:


    //! ROS NodeHandler used to manage the ROS communication
    ros::NodeHandle node;
    ros::Publisher pub_depth;
    ros::Publisher pub_rgb;

    std::string depth_topic_name  = "rs_depth_image";
    std::string rgb_topic_name  = "rs_rgb_image";


    rs2::pipeline pipe;
    rs2::config config_rs;
    rs2::frameset frameset;
    rs2_stream align_to = RS2_STREAM_DEPTH;

    rs2::align align_to_color;

    // BallDetector ball_detect;

    const int width = 1280;
    const int height = 720;

};

#endif
