cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME realsenseXavier)

project(${PROJECT_NAME})

##project(rrt_exploration)
##SET_TARGET_PROPERTIES([prrt_exploration] PROPERTIES LINKER_LANGUAGE C)
#add_definitions(-std=c++11)
set(CMAKE_BUILD_TYPE "Release")
set(CMAKE_CXX_FLAGS "-std=c++11")
set(CMAKE_CXX_FLAGS_RELEASE "-O3 -Wall -g")

set(REALSENSE_XAVIER_SOURCE_DIR
    src/sources)

set(REALSENSE_XAVIER_INCLUDE_DIR
    src/include
)

# Here every .cpp file

set(REALSENSE_XAVIER_SOURCE_FILES
    ${REALSENSE_XAVIER_SOURCE_DIR}/main_process.cpp
    ${REALSENSE_XAVIER_SOURCE_DIR}/realsense_process.cpp
    #${REALSENSE_XAVIER_SOURCE_DIR}/ball_detector.cpp
)

# Here every .h file

set(REALSENSE_XAVIER_HEADER_FILES
    ${REALSENSE_XAVIER_INCLUDE_DIR}/realsense_process.h
    #${REALSENSE_XAVIER_INCLUDE_DIR}/ball_detector.h
    #${REALSENSE_XAVIER_INCLUDE_DIR}/detector_wrapper.h
)

# add_subdirectory(libs/librealsense)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages

find_package(catkin REQUIRED COMPONENTS
    roscpp
    std_msgs
    rospy
    opencv_apps
    geometry_msgs
    sensor_msgs
    cv_bridge
    image_transport
    tf
    tf_conversions
    message_generation
    referenceFrames
    aruco_eye_msgs
    robot_process
    lib_pugixml
    lib_cvgutils
    )



#if ompl

#find_package(ompl REQUIRED)

# find_package(OpenGL REQUIRED)
find_package(OpenCV REQUIRED)
find_package(realsense2 REQUIRED)

# find_package(Eigen3 REQUIRED)

##add_definitions(${EIGEN_DEFINITIONS})

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

# set(realsense2_DIR /home/miguel/workspace/aerostack_packages/include/librealsense/build)

# find_package(realsense2 REQUIRED PATHS /home/miguel/workspace/aerostack_packages/include/librealsense/CMake)

#add_message_files(
#    FILES
#    )

#add_service_files(
#  FILES
#)

# generate_messages(
#     DEPENDENCIES
#
#     )

###################################
## catkin specific configuration ##


catkin_package(
    CATKIN_DEPENDS roscpp std_msgs
    opencv_apps
    geometry_msgs
    sensor_msgs
    cv_bridge
    image_transport
    tf
    tf_conversions
    message_runtime
    referenceFrames
    aruco_eye_msgs
    robot_process
    lib_pugixml
    lib_cvgutils
    DEPENDS OpenCV
    # DEPENDS OpenCV Eigen3

    )


###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)


include_directories(
    ${REALSENSE_XAVIER_INCLUDE_DIR}
    ${catkin_INCLUDE_DIRS}
    # ${Eigen3_INCLUDE_DIRS}
    ${OpenCV_INCLUDE_DIRS}
    ${realsense2_INCLUDE_DIR}
    )

add_library(${PROJECT_NAME} ${REALSENSE_XAVIER_SOURCE_FILES} ${REALSENSE_XAVIER_HEADER_FILES})
add_dependencies(${PROJECT_NAME} ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES} ${OpenCV_LIBS} ${realsense2_LIBRARY} )

add_executable(main_realsense src/sources/main_process.cpp)
add_dependencies(main_realsense ${catkin_EXPORTED_TARGETS})
target_link_libraries(main_realsense ${PROJECT_NAME} ${catkin_LIBRARIES}  ${OpenCV_LIBRARY} ${realsense2_LIBRARY})
